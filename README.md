tfplantool
==========

tfplantool is a simple Go CLI application to interact with Terraform plan files.

Usage
-----

```
tfplantool help
```

Notes
-----

tfplantool must be compiled against each version of Terraform it will be used
with. There are currently static binaries built for 0.13.2 and 0.12.29 for each
commit.
