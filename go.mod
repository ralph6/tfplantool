module gitlab.com/mattkasa/tfplantool

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/hashicorp/terraform v0.13.2
	github.com/urfave/cli/v2 v2.2.0
	github.com/zclconf/go-cty v1.6.1
)
